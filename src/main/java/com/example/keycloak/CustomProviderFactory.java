package com.example.keycloak;

import org.keycloak.provider.ProviderFactory;
import org.keycloak.provider.Spi;

/**
 * Factory class for CustomProvider.
 */
public class CustomProviderFactory implements ProviderFactory<CustomProvider> {

    /**
     * Creates an instance of CustomProvider.
     *
     * @return an instance of CustomProvider.
     */
    @Override
    public CustomProvider create(KeycloakSession session) {
        return new CustomProvider(session);
    }

    /**
     * The ID of the provider.
     *
     * @return the ID of the provider.
     */
    @Override
    public String getId() {
        return "custom-provider";
    }

    // Optionally override other methods from ProviderFactory interface
    // for example, init(), postInit
