# Start from a Maven base image
FROM maven:3.6.3-jdk-11 as builder

# Copy your source code into the image
COPY src /usr/src/app/src
COPY pom.xml /usr/src/app

# Set the working directory
WORKDIR /usr/src/app

# Build the project
RUN mvn clean install

# Second stage: setup the Keycloak environment
FROM jboss/keycloak:latest

# Copy the plugin jar from the builder stage
COPY --from=builder /usr/src/app/target/keycloak-custom-provider.jar /opt/jboss/keycloak/standalone/deployments/

# Set the working directory to the Keycloak home
WORKDIR /opt/jboss/keycloak
